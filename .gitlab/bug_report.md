---
nom : Bug report
about : Créez un incident pour nous aider à nous améliorer
titre : ''
labels : ''

---

**Décrire le bug**
Une description claire et concise de ce qu'est le bug.

**Pour Reproduire**
Étapes pour reproduire le comportement :

1. Allez dans '...'.
2. Cliquez sur '....'.
3. Faites défiler vers le bas jusqu'à "....".
4. Voir l'erreur

**Comportement attendu**
Une description claire et concise de ce que vous attendez.

**Captures d'écran**
Le cas échéant, ajoutez des captures d'écran pour aider à expliquer votre problème.

**Versions (veuillez compléter les informations suivantes):**

- SYSTÈME D'EXPLOITATION : [par exemple, Ubuntu 18.04].

**Contexte supplémentaire**
Ajoutez tout autre contexte concernant le problème ici.